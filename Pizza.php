<?php
// Pareil qu'en dessous mais en representation binaire
//define('TOPPING_PEPPERONI', 0b00001);
//define('TOPPING_ARTICHAUT', 0b00010);
//define('TOPPING_ANANAS',    0b00100);
//define('TOPPING_TOFU',      0b01000);
//define('TOPPING_MERGUEZ',   0b10000);

// representation decimale
define('TOPPING_PEPPERONI', 1);
define('TOPPING_ARTICHAUT', 2);
define('TOPPING_ANANAS',    4);
define('TOPPING_TOFU',      8);
define('TOPPING_MERGUEZ',  16);

echo whatAreMyToppings(0);
echo whatAreMyToppings(TOPPING_TOFU | TOPPING_PEPPERONI | TOPPING_MERGUEZ);
echo whatAreMyToppings(TOPPING_TOFU | TOPPING_PEPPERONI | TOPPING_MERGUEZ | TOPPING_ANANAS | TOPPING_ARTICHAUT);

/**
 * Retourne la liste des eventuelles garnitures de la pizza
 *
 * @param int $pizza
 *
 * @return string
 */
function whatAreMyToppings(int $pizza): string
{
    if (0 === $pizza) {
        return "Votre pizza ne contient aucune garniture.".PHP_EOL;
    }
    $bufferToppings = "Il y a dans la pizza :".PHP_EOL;
    if (TOPPING_PEPPERONI === (TOPPING_PEPPERONI & $pizza)) {
        $bufferToppings .= "* Pepperoni".PHP_EOL;
    }
    if (TOPPING_MERGUEZ === (TOPPING_MERGUEZ & $pizza)) {
        $bufferToppings .= "* Merguez".PHP_EOL;
    }
    if (TOPPING_ANANAS === (TOPPING_ANANAS & $pizza)) {
        $bufferToppings .= "* Ananas".PHP_EOL;
    }
    if (TOPPING_TOFU === (TOPPING_TOFU & $pizza)) {
        $bufferToppings .= "* Tofu".PHP_EOL;
    }
    if (TOPPING_ARTICHAUT === (TOPPING_ARTICHAUT & $pizza)) {
        $bufferToppings .= "* Merguez".PHP_EOL;
    }
    return $bufferToppings;
}
