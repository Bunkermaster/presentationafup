Présentation AFUP Binary Fun
===
20190917
---

# Besoin

Le besoin est de stocker une abstraction de pizza qui nous permettra de définir quelles garnitures ont été choisies. 

La liste des garnitures est définie et chaque garniture ne peut être choisie qu’une fois.

#Garnitures

* Pepperoni
* Artichaut
* Ananas
* Tofu
* Merguez

# Solution

Pizza!